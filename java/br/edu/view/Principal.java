package br.edu.view;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.config.ConfigScope;
import com.db4o.config.EmbeddedConfiguration;
import com.db4o.query.Predicate;
import com.db4o.query.QueryComparator;

import br.edu.model.Carro;

public class Principal {

	public static void main(String[] args) throws NumberFormatException, IOException {
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		EmbeddedConfiguration configuration = Db4oEmbedded.newConfiguration();
		configuration.file().generateUUIDs(ConfigScope.GLOBALLY);
				
		//Criando ou conectando com o banco
		ObjectContainer dbCarro = Db4oEmbedded.openFile(configuration, "/home/welder/PessoaCarros/carros.db4o");
		ObjectContainer dbPessoa = Db4oEmbedded.openFile(configuration, "/home/welder/PessoaCarros/pessoas.db4o");
		ObjectContainer dbPessoaCarros = Db4oEmbedded.openFile(configuration, "/home/welder/PessoaCarros/pessoaCarros.db4o");
		
		/*
		 * Carros inseridos
		Carro fusca = new Carro();
		fusca.setPlaca("XAB-0001");
		fusca.setNumeroRevisoes(4);
		
		dbCarro.store(fusca);
		
		Carro brasilia = new Carro();
		brasilia.setPlaca("XYZ-0002");
		
		dbCarro.store(brasilia);
		
		Carro chevette = new Carro();
		chevette.setPlaca("ABD-0003");
		chevette.setNumeroRevisoes(2);
		
		dbCarro.store(chevette);
		*/
		
		/*
		 * Pessoas inseridas
		Pessoa anderson = new Pessoa();
		anderson.setNome("Anderson");
		
		dbPessoa.store(anderson);
		
		Pessoa zelia = new Pessoa();
		zelia.setNome("Zelia");
		
		dbPessoa.store(zelia);
		*/
		
		/* 
		 * Adicionando carros as pessoas
        
		Pessoa nubia = new Pessoa();
        nubia.setNome("Núbia");
        
        Carro gol = new Carro();
        gol.setPlaca("HBX-7079");
        gol.setNumeroRevisoes(5);
        
        Carro fiat = new Carro();
        fiat.setPlaca("ALN-9030");
        fiat.setNumeroRevisoes(10);
        
        nubia.adicionarCarro(fiat);
        nubia.adicionarCarro(gol);
        
        dbPessoaCarros.store(nubia);
		*/
		
		/*
		 * Buscando um carro e mostrando o id
		 */
		ObjectSet<Carro> result = dbCarro.query(new Predicate<Carro>(){

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public boolean match(Carro carro) {
				// TODO Auto-generated method stub
				return carro.getPlaca().contains("X") && carro.getNumeroRevisoes() <= 2;
			}
			
		},new QueryComparator<Carro>(){

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			/**
			 * Ordenação ascendente (menor para maior)
			 */
			public int compare(Carro carroUm, Carro carroDois) {
				// TODO Auto-generated method stub
				return carroUm.getPlaca().compareTo(carroDois.getPlaca()) ;
			}
			
		});
		
		while (result.hasNext()) {
			Carro temp = result.next();
			System.out.println("Id: "+dbCarro.ext().getID(temp));
			System.out.println("Placa: "+temp.getPlaca());
			System.out.println("Revisões: "+temp.getNumeroRevisoes());
			System.out.println();
		}
				
		dbCarro.close();
		dbPessoa.close();
		dbPessoaCarros.close();
		
		System.out.println("Digite um número para sair do sistema: ");
		Integer i = Integer.parseInt(br.readLine());
		System.out.println(i);
		// make something here!!
		br.close();

	}

}
