package br.edu.model;

import java.util.HashSet;
import java.util.Set;

import com.db4o.config.annotations.Indexed;

public class Pessoa {

	@Indexed
	private String nome;
	
	private Set<Carro> carros = new HashSet<Carro>();

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public Set<Carro> getCarros(){
		return carros;
	}
	
	public void adicionarCarro(Carro carro){
		carros.add(carro);
	}
		
}
