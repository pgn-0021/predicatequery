package br.edu.model;

import com.db4o.config.annotations.Indexed;

public class Carro {

	@Indexed
	private String placa;

	private int numeroRevisoes=0;
	
	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public int getNumeroRevisoes() {
		return numeroRevisoes;
	}

	public void setNumeroRevisoes(int numeroRevisoes) {
		this.numeroRevisoes = numeroRevisoes;
	}
	
}
